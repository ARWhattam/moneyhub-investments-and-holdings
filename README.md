# Moneyhub Tech Test - Investments and Holdings

## Instructions

To develop against all the services each one will need to be started in each service run

```bash
npm start
or
npm run develop
```

The develop command will run nodemon allowing you to make changes without restarting

The services will try to use ports 8081, 8082 and 8083

Use Postman collection "Moneyhub Task.postman_collection" to trigger endpoints.

To run unit tests run

```bash
npm test
```

## Routes

Investments - localhost:8081
- `/investments` get all investments
- `/investments/:id` get an investment record by id
- `/investments/export` expects a csv formatted text input as the body

Financial Companies - localhost:8082
- `/companies` get all companies details
- `/companies/:id` get company by id

Admin - localhost:8083
- `/investments/:id` get an investment record by id
- `/report` create report and send to /investments/export

## Q/A

1. How might you make this service more secure?
    - HTTPS.
    - Remove console logging of unsainitized inputs (security-node/detect-crlf).

2. How would you make this solution scale to millions of records?
    - Add parameters to the report endpoint to allow filtering of data, for example by date or total investment.
    - Return paginated data.
    - Cache data returned from other APIs, if appropriate for use case.

3. What else would you have liked to improve given more time?
    - Add integration tests using Postman collection.
    - Switch from deprecated Request package to Axios. This would allow inbuilt use of promises, simplifying the code.
    - If given even more time, switch from CommonJS to ES modules. This would allow use of the latest Got package which is a highly recommended alternative to Request.
    - Use TypeScript.
    - Suppress logging when running tests.
