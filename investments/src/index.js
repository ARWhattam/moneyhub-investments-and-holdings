const express = require("express")
const bodyParser = require("body-parser")
const config = require("config")
const investments = require("./data")
const R = require("ramda")

const app = express()

const csvParser = bodyParser.text({limit: "10mb", type: "text/csv"})

app.get("/investments", (_req, res) => {
  res.send(investments)
})

app.get("/investments/:id", (req, res) => {
  const {id} = req.params
  const investment = R.filter(R.propEq("id", id), investments)
  res.send(investment)
})

app.post("/investments/export", csvParser, (req, res) => {
  console.log("Body received", req.body)
  res.sendStatus(204)
})

app.listen(config.port, (err) => {
  if (err) {
    console.error("Error occurred starting the server", err)
    process.exit(1)
  }
  console.log(`Server running on port ${config.port}`)
})
