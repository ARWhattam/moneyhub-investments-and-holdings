const express = require("express")
const bodyParser = require("body-parser")
const config = require("config")
const request = require("request")
const R = require("ramda")

const app = express()

app.use(bodyParser.json({limit: "10mb"}))

app.get("/investments/:id", (req, res) => {
  const {id} = req.params
  request.get(`${config.investmentsServiceUrl}/investments/${id}`, (e, _r, investments) => {
    if (e) {
      console.error(e)
      res.sendStatus(500)
    } else {
      res.send(investments)
    }
  })
})

app.post("/report", async (_req, res) => {

  const investmentsRequest = new Promise((resolve, reject) => {
    request.get({
      url: `${config.investmentsServiceUrl}/investments`,
      json: true,
    }, (e, _r, investments) => {
      if (e) {
        reject(e)
      } else {
        resolve(investments)
      }
    })
  })

  const companiesRequest = new Promise((resolve, reject) => {
    request.get({
      url: `${config.companiesServiceUrl}/companies`,
      json: true,
    }, (e, _r, companies) => {
      if (e) {
        reject(e)
      } else {
        resolve(companies)
      }
    })
  })

  const [investments, companies] = await Promise.all([investmentsRequest, companiesRequest])
    .catch(e => {
      console.error(e)
      res.send(500)
    })

  const getCompanyNameById = id => R.find(c => c.id === id, companies).name

  const convertInvestmentsToCSV = R.pipe(
    R.map(R.unwind("holdings")),
    R.unnest,
    R.map(i => `${i.userId},${i.firstName},${i.lastName},${i.date},${getCompanyNameById(i.holdings.id)},${i.investmentTotal * (i.holdings.investmentPercentage)}`),
    R.join("\r\n"),
  )

  const report = convertInvestmentsToCSV(investments)

  request.post({
    url: `${config.investmentsServiceUrl}/investments/export`,
    headers: {
      "Content-Type": "text/csv",
    },
    body: report,
  }, (e) => {
    if (e) {
      console.error(e)
      res.send(500)
    } else {
      res.sendStatus(201)
    }
  })
})

app.listen(config.port, (err) => {
  if (err) {
    console.error("Error occurred starting the server", err)
    process.exit(1)
  }
  console.log(`Server running on port ${config.port}`)
})

module.exports = app
