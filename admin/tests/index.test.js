const chai = require("chai")
const chaiHttp = require("chai-http")
const sinon = require("sinon")

const request = require("request")
const app = require("../src/index")

chai.use(chaiHttp)
chai.should()

const mockInvestment = {
  "id": "2",
  "userId": "2",
  "firstName": "Sheila",
  "lastName": "Aussie",
  "investmentTotal": 20000,
  "date": "2020-01-01",
  "holdings": [{"id": "1", "investmentPercentage": 0.5}, {"id": "2", "investmentPercentage": 0.5}]
}

const mockCompanies = [
  {
    "id": "1",
    "name": "The Big Investment Company",
    "address": "14 Square Place",
    "postcode": "SW18UU",
    "frn": "234165",
  }, {
    "id": "2",
    "name": "The Small Investment Company",
    "address": "12 Circle Square",
    "postcode": "SW18UD",
    "frn": "773388",
  },
]

const mockReport = "2,Sheila,Aussie,2020-01-01,The Big Investment Company,10000\r\n2,Sheila,Aussie,2020-01-01,The Small Investment Company,10000"

describe("GET /investments/:id", function() {
  beforeEach(function() {
    this.get = sinon.stub(request, "get")
  })
  afterEach(function() {
    this.get.restore()
  })

  it("should return data from investments API", function(done) {
    this.get.yields(null, {}, mockInvestment)

    chai.request(app)
      .get("/investments/1")
      .end((err, res) => {
        res.statusCode.should.eql(200)
        res.headers["content-type"].should.contain("application/json")
        res.body.should.eql(mockInvestment)
        done()
      })
  })

  it("should return 500 when GET to investments API fails", function(done) {
    this.get.yields("Some error", {})

    chai.request(app)
      .get("/investments/1")
      .end((err, res) => {
        res.statusCode.should.eql(500)
        done()
      })
  })
})

describe("POST /report", function() {
  beforeEach(function() {
    this.get = sinon.stub(request, "get")
    this.post = sinon.stub(request, "post")
  })
  afterEach(function() {
    this.get.restore()
    this.post.restore()
  })

  it("should return data from investments API", function(done) {
    this.get.onCall(0).yields(null, {}, [mockInvestment])
    this.get.onCall(1).yields(null, {}, mockCompanies)
    this.post.yields(null, {})

    chai.request(app)
      .post("/report")
      .end((err, res) => {
        res.statusCode.should.eql(201)

        this.post.calledWith({
          url: "localhost:8081/investments/export",
          headers: {
            "Content-Type": "text/csv",
          },
          body: mockReport,
        })

        done()
      })
  })

  it("should return 500 when GET to investments API fails", function(done) {
    this.get.onCall(0).yields("Some error", {})
    this.get.onCall(1).yields(null, {}, mockCompanies)

    chai.request(app)
      .post("/report")
      .end((err, res) => {
        res.statusCode.should.eql(500)
        done()
      })
  })

  it("should return 500 when GET to financial companies API fails", function(done) {
    this.get.onCall(0).yields(null, {}, [mockInvestment])
    this.get.onCall(1).yields("Some error", {})

    chai.request(app)
      .post("/report")
      .end((err, res) => {
        res.statusCode.should.eql(500)
        done()
      })
  })

  it("should return 500 when POST to investments API fails", function(done) {
    this.get.onCall(0).yields(null, {}, [mockInvestment])
    this.get.onCall(1).yields(null, {}, mockCompanies)
    this.post.yields("Some error", {})

    chai.request(app)
      .post("/report")
      .end((err, res) => {
        res.statusCode.should.eql(500)
        done()
      })
  })
})
